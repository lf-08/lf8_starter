package de.szut.lf8_project.project;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long> {
    List<ProjectEntity> findByDescription(String description);

    @Query("SELECT PE from ProjectEntity PE where PE.startDate between :startDate and :endDate or PE.endDate between :startDate and :endDate")
    List<ProjectEntity> findEmployeeInProjectEntityByDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate); //todo: geändert
}
