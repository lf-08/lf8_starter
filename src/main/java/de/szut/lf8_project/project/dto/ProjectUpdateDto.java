package de.szut.lf8_project.project.dto;

import java.util.Date;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectUpdateDto {

    @Size(min = 3, message = "at least length of 3")
    private String description;

    private Long id;
    private Long customerId;
    private Long associateId;
    private String comment;
    private Date startDate;
    private Date endDate;
    private Date actualEndDate;

    @JsonCreator
    public ProjectUpdateDto(Long id, String description, Long customerId, Long associateId, String comment, Date startDate, Date endDate, Date actualEndDate) {
        this.id = id;
        this.description = description;
        this.comment = comment;
        this.customerId = customerId;
        this.associateId = associateId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.actualEndDate = actualEndDate;
    }
}
