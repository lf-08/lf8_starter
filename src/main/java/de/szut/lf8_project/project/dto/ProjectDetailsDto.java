package de.szut.lf8_project.project.dto;

import lombok.Getter;

import java.util.Date;
import java.util.List;

@Getter
public class ProjectDetailsDto extends ProjectGetDto {
    private List<Long> employeeIds;

    public ProjectDetailsDto(Long id, String description, Long customerId, Long associateId, String comment, Date startDate, Date endDate, Date actualEndDate, List<Long> employeeIds) {
        super(id, description, customerId, associateId, comment, startDate, endDate, actualEndDate);
        this.employeeIds = employeeIds;
    }
}
