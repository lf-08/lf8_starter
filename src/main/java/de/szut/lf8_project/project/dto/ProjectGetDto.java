package de.szut.lf8_project.project.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ProjectGetDto {

    private Long id;
    private String description;
    private Long customerId;
    private Long associateId;
    private String comment;
    private Date startDate;
    private Date endDate;
    private Date actualEndDate;
}

