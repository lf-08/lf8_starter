package de.szut.lf8_project.project.dto;

import java.util.Date;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectCreateDto {

    @Size(min = 3, message = "at least length of 3")
    private String description;
    private Long customerId;
    private Long associateId;
    private String comment;
    private Date startDate;
    private Date endDate;
    private Date actualEndDate;

    @JsonCreator
    public ProjectCreateDto(String description, Long customerId, Long associateId, String comment, Date startDate, Date endDate, Date actualEndDate) {
        this.description = description;
        this.customerId = customerId;
        this.associateId = associateId;
        this.comment = comment;
        this.startDate = startDate;
        this.endDate = endDate;
        this.actualEndDate = actualEndDate;
    }
}
