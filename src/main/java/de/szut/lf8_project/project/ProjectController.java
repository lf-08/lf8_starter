package de.szut.lf8_project.project;

import de.szut.lf8_project.employees.EmployeeService;
import de.szut.lf8_project.employees.QualificationDetails;
import de.szut.lf8_project.employees.Skill;
import de.szut.lf8_project.employeesInProject.EmployeeInProjectEntity;
import de.szut.lf8_project.employeesInProject.EmployeeInProjectRepository;
import de.szut.lf8_project.employeesInProject.EmployeeInProjectService;
import de.szut.lf8_project.employeesInProject.dto.EmployeeAddToProjectDto;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.project.dto.ProjectCreateDto;
import de.szut.lf8_project.project.dto.ProjectDetailsDto;
import de.szut.lf8_project.project.dto.ProjectUpdateDto;
import de.szut.lf8_project.project.dto.ProjectGetDto;
import de.szut.lf8_project.qualification.QualificationEntity;
import de.szut.lf8_project.qualification.QualificationRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "project")
public class ProjectController {
    private final ProjectService projectService;
    private final ProjectMapper projectMapper;
    private final EmployeeInProjectService employeeInProjectService;
    private final EmployeeService employeeService;
    private final EmployeeInProjectRepository employeeInProjectRepository;
    private final QualificationRepository qualificationRepository;

    public ProjectController(ProjectService projectService, ProjectMapper mappingService, EmployeeInProjectService employeeInProjectService, EmployeeService employeeService, EmployeeInProjectRepository employeeInProjectRepository, QualificationRepository qualificationRepository) {
        this.projectService = projectService;
        this.projectMapper = mappingService;
        this.employeeInProjectService = employeeInProjectService;
        this.employeeService = employeeService;
        this.employeeInProjectRepository = employeeInProjectRepository;
        this.qualificationRepository = qualificationRepository;
    }

    @Operation(summary = "creates a new project with its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping
    public ProjectGetDto create(@RequestBody @Valid ProjectCreateDto projectCreateDto) {
        ProjectEntity projectEntity = this.projectMapper.mapCreateDtoToEntity(projectCreateDto);
        projectEntity = this.projectService.create(projectEntity);
        return this.projectMapper.mapToGetDto(projectEntity);
    }

    @Operation(summary = "delivers a list of projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping
    public List<ProjectGetDto> findAll() {
        return this.projectService
                .readAll()
                .stream()
                .map(e -> this.projectMapper.mapToGetDto(e))
                .collect(Collectors.toList());
    }

    @Operation(summary = "delivers projects by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "project by id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/{id}")
    public ProjectDetailsDto findById(@PathVariable long id) {
        return this.projectService.getProjectDetails(id);
    }

    @Operation(summary = "deletes a Project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "delete successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteProjectById(@PathVariable long id) {
        var entity = this.projectService.readById(id);
        if (entity == null) {
            throw new ResourceNotFoundException("ProjectEntity not found on id = " + id);
        } else {
            this.projectService.delete(entity);
        }
    }

    @Operation(summary = "updates project data with its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "updated project data",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "employee unavailable",
                    content = @Content)})
    @PatchMapping
    public ProjectGetDto updateProjectById(@RequestBody @Valid ProjectUpdateDto projectUpdateDto) {
        ProjectEntity projectEntity = this.projectMapper.mapUpdateDtoToEntity(projectUpdateDto);
        Long projectId = projectEntity.getId();
        ProjectEntity originalProjectEntity = this.projectService.readById(projectId);

        var description = projectEntity.getDescription();
        var comment = projectEntity.getComment();
        var customerId = projectEntity.getCustomerId();
        var employeesId = this.projectService.findEmployeesIdByProjectId(projectId);
        var associateId = projectEntity.getAssociateId();
        var startDate = projectEntity.getStartDate();
        var endDate = projectEntity.getEndDate();
        var actualEndDate = projectEntity.getActualEndDate();

        if (originalProjectEntity != null) {
            if (description != null) {
                projectEntity.setDescription(projectEntity.getDescription());
            } else {
                projectEntity.setDescription((originalProjectEntity.getDescription()));
            }

            if (comment != null) {
                projectEntity.setComment(projectEntity.getComment());
            } else {
                projectEntity.setComment((originalProjectEntity.getComment()));
            }

            if (customerId != null) {
                projectEntity.setCustomerId(projectEntity.getCustomerId());
            } else {
                projectEntity.setCustomerId((originalProjectEntity.getCustomerId()));
            }

            if (associateId != null) {
                projectEntity.setAssociateId(projectEntity.getAssociateId());
            } else {
                projectEntity.setAssociateId((originalProjectEntity.getAssociateId()));
            }

            if (startDate != null) {
                projectEntity.setStartDate(projectEntity.getStartDate());
            } else {
                projectEntity.setStartDate((originalProjectEntity.getStartDate()));
            }

            if (endDate != null) {
                projectEntity.setEndDate(projectEntity.getEndDate());
            } else {
                projectEntity.setEndDate((originalProjectEntity.getEndDate()));
            }

            if (actualEndDate != null) {
                projectEntity.setActualEndDate(projectEntity.getActualEndDate());
            } else {
                projectEntity.setActualEndDate((originalProjectEntity.getActualEndDate()));
            }
            projectEntity = this.projectService.create(projectEntity);

            return this.projectMapper.mapToGetDto(projectEntity);
        } else {
            throw new ResourceNotFoundException("ProjectEntity not found on id = " + projectId);
        }
    }

    @Operation(summary = "find projects by description")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of projects who have the given parameter",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "project description does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/findByDescription")
    public List<ProjectGetDto> findProjectByDescription(@RequestParam String description) {
        return this.projectService
                .findByDescription(description)
                .stream()
                .map(this.projectMapper::mapToGetDto)
                .collect(Collectors.toList());
    }

    public boolean isEmployeeAvailable(long employeeId, Date startDate, Date endDate) {
        var projects = this.projectService.findByDate(startDate, endDate);
        System.out.println(projects);
        for (ProjectEntity project : projects) {
            Long projectId = project.getId();
            var employeesId = this.projectService.findEmployeesIdByProjectId(projectId);
            for (Long projectEmployeeId : employeesId) {
                if (projectEmployeeId == employeeId) {
                    return false;
                }
            }
        }
        return true;
    }

    @Operation(summary = "adds employee to project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "added employee to project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "employee qualifications were missing",
                    content = @Content)})

    @PatchMapping(value = "/{projectId}/addEmployee/{employeeId}")
    public ProjectEntity addEmployeeToProject(@PathVariable Long projectId,
                                              @PathVariable Long employeeId,
                                              @RequestBody @Valid EmployeeAddToProjectDto employeeAddToProjectDto,
                                              HttpServletRequest request) throws Exception {
        ProjectEntity projectEntity = this.projectService.readById(projectId);
        List<Long> employeeList = this.projectService.findEmployeesIdByProjectId(projectEntity.getId());

        boolean found = false;

        for (Long employeeListId : employeeList) {
            if (Objects.equals(employeeListId, employeeId)) {
                found = true;
            }
        }

        boolean isEmployeeAvailable = isEmployeeAvailable(employeeId,
                projectEntity.getStartDate(),
                projectEntity.getEndDate());

        if (!found && isEmployeeAvailable) {
            EmployeeInProjectEntity employeeInProjectEntity = new EmployeeInProjectEntity();
            employeeInProjectEntity.setEmployeeId(employeeId);
            employeeInProjectEntity.setProjectEntity(projectEntity);

            ResponseEntity<QualificationDetails> response = this.employeeService.getQualificationsByEmployeeId(employeeId, request);
            Skill[] skills = response.getBody().getSkillSet();
            Set<String> employeeSkills = Arrays.stream(skills)
                    .map(Skill::getDesignation)
                    .collect(Collectors.toSet());

            Set<String> qualifications = employeeAddToProjectDto.getQualifications();

            if (!employeeSkills.containsAll(qualifications)) {
                throw new IllegalArgumentException("Employee does not have all skills!");
            }

            List<QualificationEntity> qualificationEntities = new ArrayList<>();
            for (String qualification : qualifications) {
                QualificationEntity qualificationEntity = new QualificationEntity();
                qualificationEntity.setQualificationTitle(qualification);
                qualificationEntities.add(qualificationEntity);
            }
            qualificationEntities = qualificationRepository.saveAll(qualificationEntities);
            employeeInProjectEntity.setQualificationEntities(qualificationEntities);
            employeeInProjectRepository.save(employeeInProjectEntity);
        } else {
            throw new ResourceNotFoundException("EmployeeId not found or not available!");
        }
        return projectEntity;
    }

    @Operation(summary = "removes an employee from a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "employee removed",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = EmployeeInProjectEntity.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "employee unavailable",
                    content = @Content)})
    @PatchMapping(value = "/{projectId}/removeEmployee/{employeeId}")
    public EmployeeInProjectEntity removeEmployeeFromProject(@PathVariable long projectId, @PathVariable long employeeId) {
        return this.employeeInProjectService.removeEmployeeInProject(projectId,employeeId);
    };

    @Operation(summary = "gets all employees of a certain project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = EmployeeInProjectEntity.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping(value = "/{projectId}/employees")
    public List<EmployeeInProjectEntity> findAllEmployeesInProject(@PathVariable long projectId) {
        return this.projectService.findEmployeesByProjectId(projectId);
    }

    @Operation(summary = "gets all projects of a certain employee")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping(value = "/{employeeId}/findProjects")
    public List<ProjectEntity> findProjectsByEmployeeId(@PathVariable long employeeId) {
        return this.projectService.findProjectsByEmployeeId(employeeId);
    }
}
