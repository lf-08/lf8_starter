package de.szut.lf8_project.project;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.szut.lf8_project.employeesInProject.EmployeeInProjectEntity;
import de.szut.lf8_project.employeesInProject.EmployeeInProjectRepository;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.project.dto.ProjectDetailsDto;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {
    private final ProjectRepository repository;
    private final EmployeeInProjectRepository employeeInProjectEntityRepository;
    private final ProjectMapper projectMapper;

    public ProjectService(ProjectRepository repository, EmployeeInProjectRepository employeeInProjectEntityRepository, ProjectMapper projectMapper) {
        this.repository = repository;
        this.employeeInProjectEntityRepository = employeeInProjectEntityRepository;
        this.projectMapper = projectMapper;
    }

    public ProjectEntity create(ProjectEntity entity) {
        return this.repository.save(entity);
    }

    public List<ProjectEntity> readAll() {
        return this.repository.findAll();
    }

    public ProjectEntity readById(long id) {
        Optional<ProjectEntity> optionalQualification = this.repository.findById(id);
        if (optionalQualification.isEmpty()) {
            return null;
        }
        return optionalQualification.get();
    }

    public ProjectDetailsDto getProjectDetails(long id) {
        ProjectEntity projectEntity = readById(id);
        if (projectEntity == null) {
            throw new ResourceNotFoundException("Project entity is null!");
        }

        List<Long> employeesIdByProjectId = findEmployeesIdByProjectId(id);
        return projectMapper.mapToDetailsDto(projectEntity, employeesIdByProjectId);
    }

    public void delete(ProjectEntity entity) {
        this.repository.delete(entity);
    }

    public List<ProjectEntity> findByDescription(String description) {
        return this.repository.findByDescription(description);
    }

    public List<ProjectEntity> findByDate(Date startDate, Date endDate) {
        return this.repository.findEmployeeInProjectEntityByDate(startDate, endDate); //todo: geändert
    }

    public List<Long> findEmployeesIdByProjectId(Long projectId) {
        return this.employeeInProjectEntityRepository.findEmployeeInProjectEntityByProjectId(projectId).stream()
                .map(EmployeeInProjectEntity::getEmployeeId).collect(Collectors.toList());
    }

    public List<EmployeeInProjectEntity> findEmployeesByProjectId(Long projectId){
        return this.employeeInProjectEntityRepository.findEmployeeInProjectEntityByProjectId(projectId);
    }

    public List<ProjectEntity> findProjectsByEmployeeId(Long employeeId){
        List<EmployeeInProjectEntity> employeeInstances = this.employeeInProjectEntityRepository.findProjectsByEmployeeId(employeeId);
        List <ProjectEntity> projects = new ArrayList<>();
        for(EmployeeInProjectEntity employeeInstance : employeeInstances){
            projects.add(employeeInstance.getProjectEntity());
        }
        return projects;
    }
}
