package de.szut.lf8_project.project;

import de.szut.lf8_project.project.dto.ProjectCreateDto;
import de.szut.lf8_project.project.dto.ProjectDetailsDto;
import de.szut.lf8_project.project.dto.ProjectGetDto;
import de.szut.lf8_project.project.dto.ProjectUpdateDto;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

@Service
public class ProjectMapper {

    public ProjectGetDto mapToGetDto(ProjectEntity entity) {
        return new ProjectGetDto(entity.getId(),
                entity.getDescription(),
                entity.getCustomerId(),
                entity.getAssociateId(),
                entity.getComment(),
                entity.getStartDate(),
                entity.getEndDate(),
                entity.getActualEndDate());
    }

    public ProjectDetailsDto mapToDetailsDto(ProjectEntity entity, List<Long> employeeIds) {
        return new ProjectDetailsDto(
                entity.getId(),
                entity.getDescription(),
                entity.getCustomerId(),
                entity.getAssociateId(),
                entity.getComment(),
                entity.getStartDate(),
                entity.getEndDate(),
                entity.getActualEndDate(),
                employeeIds);
    }

    public ProjectEntity mapCreateDtoToEntity(@Valid ProjectCreateDto dto) {
        var entity = new ProjectEntity();
        entity.setId(entity.getId());
        entity.setDescription(dto.getDescription());
        entity.setComment(dto.getComment());
        entity.setCustomerId(dto.getCustomerId());
        entity.setEndDate(dto.getEndDate());
        entity.setActualEndDate(dto.getActualEndDate());
        entity.setAssociateId(dto.getAssociateId());
        entity.setStartDate(dto.getStartDate());
        return entity;
    }

    public ProjectEntity mapUpdateDtoToEntity(ProjectUpdateDto dto) {
        var entity = new ProjectEntity();
        entity.setId(dto.getId());
        entity.setDescription(dto.getDescription());
        entity.setComment(dto.getComment());
        entity.setCustomerId(dto.getCustomerId());
        entity.setAssociateId(dto.getAssociateId());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        entity.setActualEndDate(dto.getActualEndDate());
        return entity;
    }
}
