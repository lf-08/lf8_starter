package de.szut.lf8_project.employees;

import lombok.Data;

@Data
public class QualificationDetails {
    private String surname;
    private String firstname;
    private Skill[] skillSet;
}
