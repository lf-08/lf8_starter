package de.szut.lf8_project.employees;

import lombok.Data;

@Data
public class Skill {
    private String designation;
}
