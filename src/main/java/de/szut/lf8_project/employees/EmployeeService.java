package de.szut.lf8_project.employees;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Service
public class EmployeeService {
    public ResponseEntity<Employee> getEmployeeById(long id, HttpServletRequest request) {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", request.getHeader("Authorization"));
        HttpEntity<Employee> entity = new HttpEntity<>(headers);
        return template.exchange("https://employee.szut.dev/employees/" + id, HttpMethod.GET, entity, Employee.class);
    }

    public ResponseEntity<Employee[]> getAllEmployees(HttpServletRequest request) {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", request.getHeader("Authorization"));
        HttpEntity<Employee> entity = new HttpEntity<>(headers);
        return template.exchange("https://employee.szut.dev/employees", HttpMethod.GET, entity, Employee[].class);
    }

    public ResponseEntity<QualificationDetails> getQualificationsByEmployeeId(long id, HttpServletRequest request) {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", request.getHeader("Authorization"));
        HttpEntity<Employee> entity = new HttpEntity<>(headers);
        return template.exchange("https://employee.szut.dev/employees/" + id +"/qualifications", HttpMethod.GET, entity, QualificationDetails.class);
    }
}
