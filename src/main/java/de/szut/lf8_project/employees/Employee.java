package de.szut.lf8_project.employees;

import lombok.Data;

@Data
public class Employee {
    private long id;
    private String lastName;
    private String firstName;
    private String street;
    private String postcode;
    private String city;
    private String phone;
}
