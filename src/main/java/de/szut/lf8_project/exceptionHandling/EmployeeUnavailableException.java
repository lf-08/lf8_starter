package de.szut.lf8_project.exceptionHandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE) //todo: check http.status
public class EmployeeUnavailableException extends RuntimeException {
    public EmployeeUnavailableException(String message) {
        super(message);
    }
}
