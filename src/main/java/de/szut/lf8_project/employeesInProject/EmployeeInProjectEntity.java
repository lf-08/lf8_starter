package de.szut.lf8_project.employeesInProject;

import de.szut.lf8_project.project.ProjectEntity;
import de.szut.lf8_project.qualification.QualificationEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "employeeInProject")
public class EmployeeInProjectEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long employeeId;

    @ManyToOne
    private ProjectEntity projectEntity;

    @OneToMany(cascade = CascadeType.ALL)
    private List<QualificationEntity> qualificationEntities;

}

