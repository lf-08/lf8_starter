package de.szut.lf8_project.employeesInProject.dto;

import de.szut.lf8_project.qualification.QualificationEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ElementCollection;
import java.util.List;


@AllArgsConstructor
@Getter
@Setter
public class EmployeeGetDto {

    private long employeeId;
    @ElementCollection
    private List<QualificationEntity> qualificationEntities;
}

