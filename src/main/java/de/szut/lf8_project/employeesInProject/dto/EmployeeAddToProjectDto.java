package de.szut.lf8_project.employeesInProject.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
public class EmployeeAddToProjectDto {
    private Set<String> qualifications;
}
