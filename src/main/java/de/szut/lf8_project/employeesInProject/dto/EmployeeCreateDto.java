package de.szut.lf8_project.employeesInProject.dto;


import de.szut.lf8_project.qualification.QualificationEntity;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.ElementCollection;
import java.util.List;

@Getter
@Setter
public class EmployeeCreateDto {

    private long employeeId;
    @ElementCollection
    private List<QualificationEntity> qualificationEntities;

    public EmployeeCreateDto(long employeeId, List<QualificationEntity> qualificationEntities, boolean isAvailable) {
        this.employeeId = employeeId;
        this.qualificationEntities = qualificationEntities;
    }

}
