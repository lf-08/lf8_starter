package de.szut.lf8_project.employeesInProject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeInProjectRepository extends JpaRepository<EmployeeInProjectEntity, Long> {

    @Query("SELECT EEP from EmployeeInProjectEntity EEP where EEP.projectEntity.id = :projectId")
    List<EmployeeInProjectEntity> findEmployeeInProjectEntityByProjectId(@Param("projectId") Long projectId);

    @Query("SELECT EEP from EmployeeInProjectEntity EEP where EEP.projectEntity.id = :projectId and EEP.employeeId = :employeeId")
    EmployeeInProjectEntity findEmployeeInProjectEntityByProjectIdAndEmployeeId(@Param("projectId") Long projectId, @Param("employeeId") Long employeeId);

    @Query("SELECT EEP from EmployeeInProjectEntity EEP where EEP.employeeId = :employeeId")
    List<EmployeeInProjectEntity> findProjectsByEmployeeId(@Param("employeeId") Long employeeId);
}
