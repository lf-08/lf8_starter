package de.szut.lf8_project.employeesInProject;

import de.szut.lf8_project.employeesInProject.dto.EmployeeCreateDto;
import de.szut.lf8_project.employeesInProject.dto.EmployeeGetDto;
import de.szut.lf8_project.project.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "employees")
public class EmployeeController {
    private final EmployeeInProjectService employeeInProjectService;
    private final EmployeeInProjectMapper employeeInProjectMapper;


    public EmployeeController(EmployeeInProjectService employeeInProjectService, EmployeeInProjectMapper mappingService, ProjectService projectService) {
        this.employeeInProjectService = employeeInProjectService;
        this.employeeInProjectMapper = mappingService;
    }

    @Operation(summary = "creates a new employee with its id") //todo: rename
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "created employee",
            content = {@Content(mediaType = "application/json",
                schema = @Schema(implementation = EmployeeGetDto.class))}),
        @ApiResponse(responseCode = "400", description = "invalid JSON posted",
            content = @Content),
        @ApiResponse(responseCode = "401", description = "not authorized",
            content = @Content)})
    @PostMapping
    public EmployeeGetDto create(@RequestBody @Valid EmployeeCreateDto employeeCreateDto) {
        //todo: qualifications raus beim Erstellen // anders lösen?
        EmployeeInProjectEntity employeeInProjectEntity = this.employeeInProjectMapper.mapCreateDtoToEntity(employeeCreateDto);
        employeeInProjectEntity = this.employeeInProjectService.create(employeeInProjectEntity);
        return this.employeeInProjectMapper.mapToGetDto(employeeInProjectEntity);
    }

    @Operation(summary = "delivers a list of employees")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "list of employees",
            content = {@Content(mediaType = "application/json",
                schema = @Schema(implementation = EmployeeGetDto.class))}),
        @ApiResponse(responseCode = "401", description = "not authorized",
            content = @Content)})
    @GetMapping
    public List<EmployeeGetDto> findAll() {
        return this.employeeInProjectService
            .readAll()
            .stream()
            .map(e -> this.employeeInProjectMapper.mapToGetDto(e))
            .collect(Collectors.toList());
    }
}
