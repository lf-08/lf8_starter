package de.szut.lf8_project.employeesInProject;

import de.szut.lf8_project.employeesInProject.dto.EmployeeCreateDto;
import de.szut.lf8_project.employeesInProject.dto.EmployeeGetDto;
import org.springframework.stereotype.Service;

@Service
public class EmployeeInProjectMapper {

    public EmployeeGetDto mapToGetDto(EmployeeInProjectEntity entity) {
        return new EmployeeGetDto(entity.getEmployeeId(), entity.getQualificationEntities());
    }

    public EmployeeInProjectEntity mapCreateDtoToEntity(EmployeeCreateDto dto) {
        var entity = new EmployeeInProjectEntity();
        entity.setEmployeeId(dto.getEmployeeId());
        entity.setQualificationEntities(dto.getQualificationEntities());
        return entity;
    }
}
