package de.szut.lf8_project.employeesInProject;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeInProjectService {
    private final EmployeeInProjectRepository repository;

    public EmployeeInProjectService(EmployeeInProjectRepository repository) {
        this.repository = repository;
    }

    public EmployeeInProjectEntity create(EmployeeInProjectEntity entity) {
        return this.repository.save(entity);
    }

    public List<EmployeeInProjectEntity> readAll() {
        return this.repository.findAll();
    }

    public EmployeeInProjectEntity readById(long id) {
        Optional<EmployeeInProjectEntity> optionalQualification = this.repository.findById(id);
        if (optionalQualification.isEmpty()) {
            return null;
        }
        return optionalQualification.get();
    }

    public EmployeeInProjectEntity removeEmployeeInProject(long projectId, long employeeId){
        EmployeeInProjectEntity employee = this.repository.findEmployeeInProjectEntityByProjectIdAndEmployeeId(projectId, employeeId);
        this.repository.delete(employee);
        return employee;
    }
}
